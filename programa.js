class Mapa {
  miVisor;
  mapaBase;
  posicionInicial = [4.710989, -74.07209];
  escalaInicial = 12;
  marcador;
  proveedorURL='http://{s}.google.com/vt/lyrs=p&x={x}&y={y}&z={z}';
  atributosProveedor={
    maxZoom: 20,
    subdomains:['mt0','mt1','mt2','mt3']
  }
  //openstreetMap: "https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png"

  constructor() {
    //setView espera un arreglo con latitud, longitud en formato decimal y un entero indicando la escala
    this.miVisor = L.map("mapid");
    this.miVisor.setView(this.posicionInicial, this.escalaInicial);
    this.mapaBase = L.tileLayer(
      this.proveedorURL,this.atributosProveedor
    );
    this.mapaBase.addTo(this.miVisor);
  }

  crearMarcador(posicion) {
    if (posicion === undefined) {
        console.warn("Colocando un marcador en una posición predeterminada");
        posicion=this.posicionInicial;      
    }
    this.marcador = L.marker(posicion);
    this.marcador.addTo(this.miVisor);    
  }
}

let miMapa = new Mapa();

miMapa.crearMarcador([4.683449,-74.106681]);
